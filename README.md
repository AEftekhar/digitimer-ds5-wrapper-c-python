# Digitimer DS5 Wrapper (C++, Python)
Designed by Amir Eftekhar (a.eftekhar@gmail.com), January 2018.

Code for wrapping the DS5 COM library for C++ and Python

DS5 installation can be found here: http://update.digitimer.com/DS5/software/V3.3.1.0/

An Interop DLL is well explained here: http://stackoverflow.com/questions/1670940/what-is-the-interop-dll

It is a COM .NET DLL assembly generated in C#. The C# sample code and DLL can be accessed once you install the software: http://update.digitimer.com/DS5/software/latest/ 

The wrapper designed here is based on the wrapper described here: http://pragmateek.com/using-c-from-native-c-with-the-help-of-ccli-v2/

Please note, that the wrapper does not currently include all DS5 control functionality. It only includes:

CheckDS5Connected: A check to see if the DS5 is connected, returns bool (0=Not connected, 1=connected)  
ToggleOutput: Enables or disables the DS5 output (input bool, if true enable, else disable output)  
Set5mA5V: Sets the DS5 input range as +-5V and output as 50mA  
SetBackLight: Toggle the backlight   

To add additional functionality, please feel free to contact me directly.