In this folder I have 3 projects

DS5LibClassLibrary is a C# class library with the appropriate functions to enable communication with a DS5
CPPDS5Lib is a C++ CLR wrapper library for bridginging the C# class library to C++
DS5LibTest is a test program to test this out

The link is as follows:


DS5 COM interface (C# interop dll) -> DS5LibClassLibrary -> CPPDS5Lib -> DS5LibTest