
#include "Stdafx.h"
#using "DS5LibAssembly.dll"
#include <msclr\auto_gcroot.h>
#include "MixedCode.h"


using namespace System::Runtime::InteropServices;

class DS5LibWrapperPrivate
{
public: msclr::auto_gcroot<DS5LibClass^> DS5LibAPI;
};

DS5LibWrapper::DS5LibWrapper()
{
	_private = new DS5LibWrapperPrivate();
	_private->DS5LibAPI = gcnew DS5LibClass();

}

bool DS5LibWrapper::CheckDS5connected()
{
	return _private->DS5LibAPI->CheckDS5connected();
}

void DS5LibWrapper::ToggleOutput(bool OnOff)
{
	_private->DS5LibAPI->ToggleOutput(OnOff);
}

void DS5LibWrapper::SetBackLight()
{
	_private->DS5LibAPI->SetBackLight();
}

void DS5LibWrapper::Set5mA5V()
{
	_private->DS5LibAPI->Set5mA5V();
}

DS5LibWrapper::~DS5LibWrapper()
{
	delete _private;

}