// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "MixedCode.h"

int main()
{
	DS5LibWrapper DS5;
	bool i = DS5.CheckDS5connected();

	std::cout << "Connected: " << i << std::endl;

	DS5.SetBackLight();
	DS5.Set5mA5V();
	DS5.ToggleOutput(true);

	getchar();
	DS5.ToggleOutput(false);
}

