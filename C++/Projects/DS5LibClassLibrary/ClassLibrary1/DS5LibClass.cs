﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

public class DS5LibClass
{
    public bool CheckDS5connected()
    {
        try
        {
            DS5Lib.Application application;
            DS5Lib.Device device;
            application = new DS5Lib.Application();
            DS5Lib.ICollection devices = application.Devices();
            device = (DS5Lib.Device)devices.get_Item(0);
            return true;
        }
        catch
        {
            return false;
        }

    }


    public void ToggleOutput(bool OnOff)
    {

        DS5Lib.Application application;
        DS5Lib.Device device;
        application = new DS5Lib.Application();
        DS5Lib.ICollection devices = application.Devices();
        device = (DS5Lib.Device)devices.get_Item(0);

        DS5Lib.ControlClass param = new DS5Lib.ControlClass();
        // By default the ControlClass is empty and applying it to a device will cause no changes to occur
        // So set the attributes to the state we want.
        if (OnOff)
        { 
            param.OutputEnable = true;
            param.AutoZero = true;
        }
        else
        { 
            param.OutputEnable = false; 
        }
        
        // Apply the ControlClasss object to the device, the device will ensure that each command is carried out in the correct order.
        // Note that this is just a request to issue the commands, the state of the device must be monitored to ensure that it is as expected.
        // for example if the auto-zero command fails due an excessive offset then output will not be enabled.
        device.Control = param;


    }

    public void Set5mA5V()
    {
        DS5Lib.Application application;
        DS5Lib.Device device;
        application = new DS5Lib.Application();
        DS5Lib.ICollection devices = application.Devices();
        device = (DS5Lib.Device)devices.get_Item(0);
        DS5Lib.ControlClass param = new DS5Lib.ControlClass();

        // By default the ControlClass is empty and applying it to a device will cause no changes to occur
        // So set the attributes to the state we want.
        param.InputVoltageRange = DS5Lib.EnInputVoltageRange.enIVR5V0;
        param.OutputCurrentRange = DS5Lib.EnOutputRange.enOR50mA;
        device.Control = param;

    }

    public void SetBackLight()
    {
        DS5Lib.Application application;
        DS5Lib.Device device;
        application = new DS5Lib.Application();
        DS5Lib.ICollection devices = application.Devices();
        device = (DS5Lib.Device)devices.get_Item(0);

        DS5Lib.ControlClass param = new DS5Lib.ControlClass();
        param.Backlight = !((DS5Lib.State)device.State).Backlight;
        device.Control = param;

    }






}

