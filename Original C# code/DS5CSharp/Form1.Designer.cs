﻿namespace DS5CSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InfoTxt = new System.Windows.Forms.TextBox();
            this.textState = new System.Windows.Forms.TextBox();
            this.OutputOFFButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // InfoTxt
            // 
            this.InfoTxt.Location = new System.Drawing.Point(12, 12);
            this.InfoTxt.Multiline = true;
            this.InfoTxt.Name = "InfoTxt";
            this.InfoTxt.Size = new System.Drawing.Size(289, 76);
            this.InfoTxt.TabIndex = 0;
            // 
            // textState
            // 
            this.textState.Location = new System.Drawing.Point(12, 94);
            this.textState.Multiline = true;
            this.textState.Name = "textState";
            this.textState.Size = new System.Drawing.Size(289, 256);
            this.textState.TabIndex = 1;
            // 
            // OutputOFFButton
            // 
            this.OutputOFFButton.Location = new System.Drawing.Point(307, 12);
            this.OutputOFFButton.Name = "OutputOFFButton";
            this.OutputOFFButton.Size = new System.Drawing.Size(240, 30);
            this.OutputOFFButton.TabIndex = 2;
            this.OutputOFFButton.Text = "Output OFF";
            this.OutputOFFButton.UseVisualStyleBackColor = true;
            this.OutputOFFButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(307, 48);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(240, 79);
            this.button2.TabIndex = 3;
            this.button2.Text = "Turns the DS5 output off. Sets the Input Voltage Range to 5V, the Output Current " +
                "Rage to 25mA. Performs an Autozero Command and then re-enables the output.";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(307, 133);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(240, 79);
            this.button3.TabIndex = 6;
            this.button3.Text = "Turns the DS5 output off. Sets the Input Voltage Range to 10V, the Output Current" +
                " Rage to 50mA. Performs an Autozero Command and then re-enables the output.";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(307, 218);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Toggle Backlight";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(307, 247);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(240, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "Toggle Buzzer";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 368);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.OutputOFFButton);
            this.Controls.Add(this.textState);
            this.Controls.Add(this.InfoTxt);
            this.Name = "Form1";
            this.Text = "DS5 C# Example";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox InfoTxt;
        private System.Windows.Forms.TextBox textState;
        private System.Windows.Forms.Button OutputOFFButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;


    }
}

