﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

// Important!
// This DS5CSharp sample project is updated during the DS5 software setup.
// If you intended to make changes to these files please copy them to another 
// location otherwise they may be overwritten by future installations of the DS5 software.

namespace DS5CSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Try to create an instance of the DS5 COM server
            try
            {
                application = new DS5Lib.Application();
            }catch(System.Exception Err){
                // Here because the DS5 Control Software installation is invalid
                MessageBox.Show("Unable to access the DS5 Control Software.\r\n\r\n" + Err.Message + "\r\n\r\nPlease reinstall the DS5 Control Software","Check installation");
                Application.Exit();
                return;
            }
            // Connect our event handlers so whenever a DS5 is connected or disconnected we are notified 
            application.OnDS5Added += new DS5Lib._IApplicationEvents_OnDS5AddedEventHandler(RefreshDS5);
            application.OnDS5Removed += new DS5Lib._IApplicationEvents_OnDS5RemovedEventHandler(RefreshDS5);

            // The DS5 server may have already been running... so check for any existing devices
            RefreshDS5();
        }

        protected override void Dispose(bool disposing)
        {
            // Cleanup DS5 
            dispose_device();           
            dispose_application();
            // Now do the standard WinForms cleanup
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        // This function cleans up a device
        // Due to the .NET lazy garbage collection when the DS5Lib.Device object is set to null 
        // the underlying COM object is not released. As Device objects can be fairly heavyweight objects
        // we need to play nice and force the release to reduce resource usage in the DS5 COM server.

        // System.Runtime.InteropServices.Marshal.ReleaseComObject() looked as if it should help with
        // this but failed to perform as expected hence we force the entire application to garbage collect
        // here instead.
        
        private void dispose_device()
        {
            // If we have a device object
            if (device != null)
            {
                // Then disconnect the event handler
                device.Changed -= device_Changed;                
                ((DS5Lib._IDeviceEvents_Event)device).Disconnected  -= device_Disconnected;   
                // Set it to null and let it be garbage collected
                device = null;
                // Force the .NET runtime to really release it.
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
            }
        }

        private void dispose_application()
        {
            // If we have an application object
            if (application != null)
            {
                // Then disconnect the event handlers
                application.OnDS5Added -= RefreshDS5;
                application.OnDS5Removed -= RefreshDS5;
                // Set it to null and let it be garbage collected
                application = null;
            }                       
        }
        private void RefreshDS5()
        {
            // There doesn't appear to be any way to guarantee that an event will not still
            // be fired even after the event has been disconnected due to the .NET interop threading model
            // So wrap the entire handler within a try/catch block
            try
            {
                // COM event handlers can be called on a different thread to that which the UI is on.
                if (InvokeRequired)
                {
                    Invoke(new voidDelegatevoid(RefreshDS5));
                }
                else
                {
                    // Now running on the UI thread
                    try
                    {
                        // Here because a DS5 has been connected, disconnected or we have forced a refresh
                        // Check to see if there are any devices connected                
                        if (application != null)
                        {
                            DS5Lib.ICollection devices = application.Devices();
                            if (devices.Count > 0)
                            {
                                // Yes, at least one DS5 connected, for simplicity lets just work with the first one we find                   
                                dispose_device();
                                device = (DS5Lib.Device)devices.get_Item(0);                                
                                // Is the DS5 firmware revision supported                            
                                if (device.IsUnsupportedFirmware)
                                {
                                    // Can't, so don't, do anything with this DS5 until the user has updated its firmware
                                    dispose_device();
                                    // Tell the user to upgrade the DS5 firmware
                                    MessageBox.Show("DS5 firmware version is too old. Please use the DS5UI software to update this devices firmware");
                                }else{
                                    // Have found a device with suitable firmware, now lets read its initial state
                                    device_Changed();
                                    // Connect to the Device Changed event so we reread the state whenever it changes  
                                    device.Changed += new DS5Lib._IDeviceEvents_ChangedEventHandler(device_Changed);
                                    // Lets also receive an event when this particular device is unplugged 
                                    // The cast here is required to disambiguate between the Disconnected property and the event
                                    ((DS5Lib._IDeviceEvents_Event)device).Disconnected += new DS5Lib._IDeviceEvents_DisconnectedEventHandler(device_Disconnected);
                                }
                            }
                            else
                            {
                                dispose_device();
                            }
                        }
                    }                    
                    catch (System.Exception)
                    {
                    }
                    // Fill the info panel with information about the device, if any. 
                    UpdateDS5Info();
                }
            }
            catch (System.Exception){}
        }

        private void UpdateDS5Info()
        {
            // Display some basic info about the DS5                
            if (device != null)
            {
                InfoTxt.Text = "Description: " + device.Description + "\r\n";
                InfoTxt.Text += "Serial Number: " + device.SerialNumber + "\r\n";
                InfoTxt.Text += "Firmware Version: " + device.FWVersionMajor + "." + device.FWVersionMinor + "." + device.BLVersionMajor + "." + device.BLVersionMinor + "\r\n";
                if (device.HasSpecialMod)
                {
                    InfoTxt.Text += "Special Mod No: " + device.SpecialModNumber + "\r\n";
                }                              
            }
            else
            {
                InfoTxt.Text = "No DS5 Connected";
            }
        }

        private void device_Disconnected()
        {
            // There doesn't appear to be any way to guarantee that an event will not still
            // be fired even after the handler has been disconnected due to the .NET interop threading model.
            // So wrap the entire handler within a try/catch block
            try
            {
                // Here because the DS5 has changed its state.
                // COM event handlers can be called on a different thread to that which the UI is on.               
                if (InvokeRequired)
                {
                    Invoke(new voidDelegatevoid(device_Disconnected));
                }
                else
                {
                    textState.Clear();
                }
            }
            catch (System.Exception) { }

        }

        private void device_Changed()
        {
            // There doesn't appear to be any way to guarantee that an event will not still
            // be fired even after the handler has been disconnected due to the .NET interop threading model.
            // So wrap the entire handler within a try/catch block
            try
            {
                // Here because the DS5 has changed its state.
                // COM event handlers can be called on a different thread to that which the UI is on.               
                if (InvokeRequired)
                {
                    Invoke(new voidDelegatevoid(device_Changed));
                }
                else
                {
                    // Now running on the UI thread                
                    // Note that the device may have been set null between the call to Invoke() and here.
                    if (device != null)
                    {
                        // Get the current state of the device
                        DS5Lib.State state = (DS5Lib.State)device.State;

                        // Show some the state of the device as a string
                        String stateStr;
                        stateStr = "device_Changed Count = " + ++onchanged_count + "\r\n";
                        stateStr += "Input Voltage Range = " + state.InputVoltageRange.ToString() + "\r\n";
                        stateStr += "Output Current Range = " + state.OutputCurrentRange.ToString() + "\r\n";
                        stateStr += "Output Enable = " + state.OutputEnable.ToString() + "\r\n"; 
                        stateStr += "Autozero = " + state.AutoZero.ToString() + "\r\n"; 
                        stateStr += "Backlight = " + state.Backlight.ToString() + "\r\n";
                        stateStr += "Buzzer = " + state.Buzzer.ToString() + "\r\n";

                        stateStr += "Input Overrange = " + state.InputSignalOverrange.ToString() + "\r\n";
                        stateStr += "OOC = " + state.OOC.ToString() + "\r\n\r\n";

                        stateStr += "PD Pulse Duration = " + state.PD.ToString() + "\r\n";
                        stateStr += "PE Pulse Energy = " + state.PE.ToString() + "\r\n";

                        stateStr += "Pk Negative Output Current = " + state.PkNegI.ToString() + "\r\n";
                        stateStr += "Pk Positive Output Current = " + state.PkPosI.ToString() + "\r\n";

                        stateStr += "Pk Negative Output Voltage = " + state.PkNegV.ToString() + "\r\n";
                        stateStr += "Pk Positive Output Voltage = " + state.PkPosV.ToString() + "\r\n";

                        stateStr += "AvH Average Pulse Current= " + state.AvH.ToString() + "\r\n";
                        stateStr += "AvL Average Non-pulse Current = " + state.AvL.ToString() + "\r\n"; 

                        
                        textState.Text = stateStr;
                    }else{
                        textState.Clear();
                    }                    
                }
            }
            catch (System.Exception) {}
        }

        int onchanged_count;
        DS5Lib.Application application;
        DS5Lib.Device device;
        delegate void voidDelegatevoid();

        private void button1_Click(object sender, EventArgs e)
        {
            if(device != null){
                DS5Lib.ControlClass param = new DS5Lib.ControlClass();
                param.OutputEnable = false; ;
                device.Control = param;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (device != null){
                // Create an instance of ControlClass.
                DS5Lib.ControlClass param = new DS5Lib.ControlClass();
                // By default the ControlClass is empty and applying it to a device will cause no changes to occur
                // So set the attributes to the state we want.
                param.OutputEnable = true;
                param.AutoZero = true;                
                param.InputVoltageRange = DS5Lib.EnInputVoltageRange.enIVR5V0;
                param.OutputCurrentRange = DS5Lib.EnOutputRange.enOR25mA;                
                // Apply the ControlClasss object to the device, the device will ensure that each command is carried out in the correct order.
                // Note that this is just a request to issue the commands, the state of the device must be monitored to ensure that it is as expected.
                // for example if the auto-zero command fails due an excessive offset then output will not be enabled.
                device.Control = param;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (device != null)
            {
                //As above, just with some slightly different settings                     
                DS5Lib.ControlClass param = new DS5Lib.ControlClass();
                param.InputVoltageRange = DS5Lib.EnInputVoltageRange.enIVR10V0;
                param.OutputEnable = true;
                param.OutputCurrentRange = DS5Lib.EnOutputRange.enOR50mA;
                param.AutoZero = true;                                
                device.Control = param;
            }
        }
       
        private void button1_Click_1(object sender, EventArgs e)
        {
            if (device != null)
            {
                DS5Lib.ControlClass param = new DS5Lib.ControlClass();
                param.Backlight = !((DS5Lib.State)device.State).Backlight;
                device.Control = param;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (device != null)
            {
                DS5Lib.ControlClass param = new DS5Lib.ControlClass();
                param.Buzzer = !((DS5Lib.State)device.State).Buzzer;
                device.Control = param;
            }
        }
        
    }
}
